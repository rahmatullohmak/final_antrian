@extends('layout.app2')


@section('judul')
    Depan
@endsection
@section('content')
    <header id="header" class="header d-flex align-items-center">

        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="" class="logo d-flex align-items-center">
                <div class="logo"
                    style="width: 63px;height: 63px;background-repeat: no-repeat;border:1px solid rgba(0, 0, 0, 0);background-size: 100%;    margin: 0 10px;background-image: url({{ asset('storage/images/' . $data->foto . '') }});">
                </div>
                <h1>{{ $data->nama_puskesmas }}<span></span></h1>
            </a>
            <nav id="navbar" class="navbar">
                <ul>

                    <li><a href="/login">login</a></li>
                </ul>
            </nav>

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

        </div>
    </header>


    <section id="hero" class="hero" style="background-color: white;">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">

                <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon-box" style="background-color: white;">
                        <p class="" style="color: black;">Panggilan antrian ke :</p>

                        <h4 style="
    font-size: 130px;
" id="jumlah">{{ $antrian }}</h4>
                    </div>
                </div>


                <div
                    class="col-lg-3 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h3 style="
    color: #008374;
">Selamaat Datang <span>Antrian 
                            {{ $data->nama_puskesmas }}</span>
                    </h3>
                    <p style="
    color: #008374;
">{{ $data->pembuka }}
                    </p>

                </div>



                <div class="col-lg-6 order-1 order-lg-2">
                    <img src="assets/img/6301.jpg" class="img-fluid" alt="" data-aos="zoom-out"
                        data-aos-delay="100">
                </div>
            </div>
        </div>



        </div>
    </section>

    <footer id="footer" class="footer">


        <div class="container mt-4">
            <div class="copyright">
                &copy; Copyright <strong><span>Morbis</span></strong>
            </div>

        </div>

    </footer>

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>





    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
        crossorigin="anonymous"></script>

    <script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
    <script>
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
        var jumlahElement = document.getElementById('jumlah');

        var pusher = new Pusher('fc202f00ac4f419bd9c2', {
            cluster: 'ap1'
        });

        var channel = pusher.subscribe('pesan');
        channel.bind('message', function(data) {
            var dataa = data["message"];
            jumlahElement.innerText = dataa;
        });
    </script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <script src="assets/js/main.js"></script>
@endsection
