@extends('layout.app2')


@section('judul')
    eror
@endsection

@section('content')
    <section id="white" class="white">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">


                <div class="col text-center">

                    <img src="{{ asset('assets/img/WhatsApp Image 2024-02-12 at 09.48.41_fed94bb4.jpg') }}" class=""
                        style="
                        width: 269px !important;
                        height: 269px !important;
                        margin: 15px;
                        border: 1px solid rgba(0, 0, 0, 0);
                    "
                        alt="barcodenya">





                </div>
                <div class="col">
                    <h2>Maaf,</h2>
                    <h5>halaman tidak ditemukan</h5>
                    <p>saat ini ada sedang di role





                        @auth
                            @if (Auth::user()->name == 'petugas')
                                petugas
                            @elseif (Auth::user()->name == 'admin')
                                admin
                            @endif
                        @endauth

                        @guest
                            pasien
                        @endguest


                        tidak dapat memuat halaman ini
                    </p>


                    </p>
                </div>
                <div class="row ">

                    <div class="col  text-center">










                        @auth
                            @if (Auth::user()->name == 'petugas')
                                <a href="/admin/petugas" class="btn btn-success" style="left: 20px;">Beranda</a>
                            @elseif (Auth::user()->name == 'admin')
                                <a href="/admin/antrian" class="btn btn-success" style="left: 20px;">Beranda</a>
                            @endif
                        @endauth

                        @guest
                            <a href="/" class="btn btn-success" style="left: 20px;">Beranda</a>
                        @endguest

                    </div>
                    <div class="col"></div>
                </div>
            </div>
        </div>



        </div>
    </section>


    <footer id="footer" class="footer">


        <div class="container mt-4">
            <div class="copyright">
                &copy; Copyright <strong><span>Morbis</span></strong>
            </div>

        </div>

    </footer>

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>








    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <script src="assets/js/main.js"></script>
@endsection
