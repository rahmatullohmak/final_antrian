<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('judul')</title>

    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link rel="icon" href="{{ asset('storage/images/' . $data->foto . '') }}" type="image/png">


    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav bg-success
             sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center">
                <div class="sidebar-brand-icon">
                    <div class="logo"
                        style="width: 63px;height: 63px;background-repeat: no-repeat;border:1px solid rgba(0, 0, 0, 0);background-size: 100%;    margin: 0 5px;background-image: url({{ asset('storage/images/' . $data->foto . '') }});">
                    </div>
                </div>
                <div class="sidebar-brand-text mx-3"> {{ $data->nama_puskesmas }}</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/admin/antrian">

                    <span>Antrian</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/costum">

                    <span>Costum</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/ubah/user/admin">

                    <span>Ubah admin</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/ubah/user/petugas">

                    <span>Ubah petugas</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">



            <!-- Heading -->


            <!-- Nav Item - Pages Collapse Menu -->


            <!-- Nav Item - Utilities Collapse Menu -->

            <!-- Divider -->


            <!-- Heading -->


            <!-- Nav Item - Pages Collapse Menu -->


            <!-- Nav Item - Charts -->


            <!-- Divider -->


            <!-- Sidebar Message -->

        </ul>
        @yield('content')


</body>

</html>
