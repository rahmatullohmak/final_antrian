@extends('layout.app2')


@section('judul')
    Pilih Metode
@endsection
@section('content')
    <header id="header" class="header d-flex align-items-center">
        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="" class="logo d-flex align-items-center">
                <div class="logo"
                    style="width: 63px;    margin: 0 10px;height: 63px;background-repeat: no-repeat;border:1px solid rgba(0, 0, 0, 0);background-size: 100%;background-image: url({{ asset('storage/images/' . $data->foto . '') }});">
                </div>
                <h1>{{ $data->nama_puskesmas }}<span></span></h1>
            </a>
            <nav id="navbar" class="navbar">
                <ul>

                    <li><a href="/logout_petugas">logout</a></li>
                </ul>
            </nav>

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

        </div>
    </header>


    <section id="hero" class="white">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">

                <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200"
                    style="
    border: 2px solid green;
">
                    <div class="icon-box teks-center" style="
    margin-top: 56px;
    text-align: center;
">

                        <h4 class="title" style="
    font-size: 37px;
">Antrian ke </h4>
                        <h4 class="title" style="
    font-size: 8pc;
"> {{ $item->nomer + 1 }}

                        </h4>
                    </div>
                </div>


                <div
                    class="col-lg-5 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h2>Metode Pendaftaraan QR: <span></span></h2>
                    <p>1. pasien diwajibkan untuk melakukan pendaftaran menggunakan sistem antrian Pusekesmas melalui scan
                        kode QR
                        <BR>
                        2. Gunakan kamera ponsel atau aplikasi Google Scan untuk memindai kode QR yang tersedia dilokasi
                        pendaftaran. <br>
                        3. Hasil scan akan mengarah ke browser dan akan menunjukan

                    </p>

                    <h2>Metode Pendaftaraan cetak: <span></span></h2>
                    <p>1.jika pasien memilih metode cetak maka harus mengklik tombol cetak maka dengan sendirinya akan
                        mengarah
                        ke perangkat print dan mencetak nomor antrian tersebut
                    </p>
                </div>

                <div class="col-lg-4 ">
                    <p>Pilih metode antrian</p>
                    @if ($izin->izin == true)
                        <div class="card"
                            style="width: 20rem;border: 2px solid green;height: 15rem;background-color: #e2e2e200;">
                            <a href="/barcode/{{ $item->nomer }}/{{ $item->kode }}">
                                <div class="card-body">
                                    <div class="img" style="margin-left: 1cm;">
                                        <img src="{{ asset('assets/img/barcod.jpeg') }}" class="" alt="...">
                                    </div>


                                </div>
                            </a>
                        </div>
                        <br>

                        <div class="card"
                            style="width: 20rem;height: 15rem;border: 2px solid green;background-color: #b0aeae00;">
                            <a href="/struk">
                                <div class="card-body">
                                    <div class="img" style="margin-left: 1cm;">
                                        <img src="{{ asset('assets/img/print.jpeg') }}" class="" alt="...">
                                        <h6 class="card-subtitle mb-2 text-body-secondary">

                                        </h6>
                                    </div>
                            </a>



                        </div>
                    @else
                        <div class="card"
                            style="width: 20rem;height: 15rem;border: 2px solid green;background-color: #b0aeae00;">

                            <div class="card-body">
                                <div class="img" style="margin-left:auto;">

                                    <h6 class="card-subtitle align-middle mb-2 text-body-secondary text-center">
                                        Maaf antrian sedang di stop
                                    </h6>
                                </div>




                            </div>
                        </div>
                    @endif






                </div>


            </div>


        </div>
        </div>



        </div>
    </section>

    <footer id="footer" class="footer">


        <div class="container mt-4">
            <div class="copyright">
                &copy; Copyright <strong><span>Morbis</span></strong>
            </div>

        </div>

    </footer>

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>




    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
        crossorigin="anonymous"></script>



    <script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
    <script>
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('fc202f00ac4f419bd9c2', {
            cluster: 'ap1'
        });

        var channel = pusher.subscribe('status');
        channel.bind('stop', function(data) {
            location.reload();
        });
    </script>

    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
    <script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>
    <script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>
@endsection
