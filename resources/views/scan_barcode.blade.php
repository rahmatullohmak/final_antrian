@extends('layout/app2')

@section('judul')
    scan barcode
@endsection


@section('content')

    <header id="header" class="header d-flex align-items-center">

        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="" class="logo d-flex align-items-center">
                <div class="logo"
                    style="width: 63px;height: 63px;background-repeat: no-repeat;border:1px solid rgba(0, 0, 0, 0);background-size: 100%;    margin: 0 10px;background-image: url({{ asset('storage/images/' . $data->foto . '') }});">
                </div>
                <h1>{{ $data->nama_puskesmas }}<span></span></h1>
            </a>
            <nav id="navbar" class="navbar">
                <ul>

                    <li><a href="/logout_petugas">logout</a></li>
                </ul>
            </nav>

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

        </div>
    </header>




    <section id="white" class="white">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">


                <div class="col text-center">
                    <div class="visible-print text-center">
                        {{-- {!! QrCode::size(100)->generate('localhost:8000/code_barcode/{{ $item->nomer }}/{{ $item->kode }}') !!} --}}


                        {{ QrCode::generate('https://'. $server.'/barcode_nomer/' . $item->nomer . '/' . $item->kode) }}
                        <p>Scan </p>
                    </div>


                    <div class="row ">
                        <div class="col m-4">

                            {{-- @if ($item->nomer > 1) --}}
                            <a href="/admin/petugas" class="btn btn-success">Selesai</a>
                            {{-- @else
                                <a href="/tambah" class="btn btn-success">Selesai</a>
                            @endif --}}

                        </div>
                    </div>


                </div>
                <div class="col">
                    <h2>Perhatikan cara penggunaanya :</h2>
                    <h5>1.Arahkan Kamera Android atau IPhone ke QR Code</h5>
                    <p>arahkan kamera foto kamu ke QR Code yang ingin kamu scan.
                        Aplikasi akan dengan otomatis melakukan proses pemindaian akan barcode.</p>
                    <h5>2.Menuju Link yang Ditunjukkan oleh Aplikasi</h5>
                    <p>Setelah melakukan proses pemindaian, aplikasi scanner QR Code akan
                        mengarahkan kamu pada link website atau media
                        yang telah disiapkan oleh pembuat barcode</p>
                    <h5>3.Kemudian Silahkan klik selesai</h5>

                    </p>
                </div>

            </div>
        </div>



        </div>
    </section>

    <footer id="footer" class="footer">


        <div class="container mt-4">
            <div class="copyright">
                &copy; Copyright <strong><span>Morbis</span></strong>
            </div>

        </div>

    </footer>

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>

    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
    <script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>
    <script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

    <script src="{{ asset('assets/js/main.js') }}"></script>
@endsection
