<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class costum extends Model
{
    use HasFactory;

    protected $table = 'costum';

    protected $fillable = ['nama_puskesmas', 'pembuka', 'foto'];
}
