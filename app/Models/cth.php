<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cth extends Model
{
    use HasFactory;
    protected $table = 'cth';
    protected $fillable = ['nomer', 'kode'];

}
