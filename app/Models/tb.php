<?php

namespace App\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tb extends Model
{
    use HasFactory;

    protected $table = 'antrian';
    protected $fillable = ['nomer', 'kode'];

}
