<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\tb;
use Illuminate\Support\Facades\DB;
use App\Models\izin;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\costum;
use Illuminate\Support\Facades\Storage;
use App\Models\cth;
use Illuminate\Support\Facades\Validator;

use App\Events\message;
use App\Events\stop;

// use tb;

class antrian extends Controller
{
    //


    public function awal()
    {
        $countTrueStatus = tb::where('status', true)->count();





        if ($countTrueStatus == false) {
            $countTrueStatus = 0;
        }
        // dd($countTrueStatus);

        $data = costum::find(1);
        return view('jumlah_antrian', ['data' => $data, 'antrian' => $countTrueStatus]);



    }
    public function login()
    {
        $data = costum::find(1);
        return view('login', ['data' => $data,]);
    }

    public function aut(request $request)
    {





        $messages = [
            'required' => 'Kolom :attribute harus diisi.',
            'string' => 'Kolom :attribute harus berupa teks.',
            'max' => 'Kolom :attribute tidak boleh lebih dari :max karakter.',
            'email' => 'Format :attribute tidak valid.',
            'unique' => ':attribute sudah digunakan.',
            'min' => 'Kolom :attribute harus memiliki minimal :min karakter.',
            'confirmed' => 'Konfirmasi :attribute tidak cocok.'
        ];
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string|max:255',
            'pasword' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return redirect('/login')
                ->withErrors($validator)
                ->withInput();
        }


        $data = [

            'name' => $request['nama']
            ,
            'password' => $request['pasword']

        ];


        if (auth::attempt($data)) {
            if (Auth::user()->name == 'admin') {
                return redirect('/admin/antrian');
            } else if (Auth::user()->name == 'petugas') {
                return redirect('/admin/petugas');
            }

        } else {
            return redirect('/login')->withErrors(['nama  atau password salah']);
        }



    }


    ///petugas


    public function petugas_antrian()
    {
        $read = tb::count();


        // dd($read);

        if ($read < 1) {
            // $read = tb::count();
            // $flight = new tb;
            // $jumlah = tb::count() + 1;
            // $randomNumber = rand(1, 1000);

            // $flight->nomer = $jumlah;
            // $flight->kode = $randomNumber;
            // $flight->status = false;
            // $flight->save();
            $izin = izin::find(1);
            $data = costum::find(1);
            $hasil = cth::where('nomer', 0)->first();

            // dd($hasil);


            return view('pilih_metode', ['nomer' => $read, 'data' => $data, 'izin' => $izin, 'item' => $hasil]);

        } else {


            // $read = tb::count() + 1;

            $izin = izin::find(1);
            $data = costum::find(1);
            $hasil = tb::where('nomer', $read)->first();


            return view('pilih_metode', ['nomer' => $read, 'data' => $data, 'izin' => $izin, 'item' => $hasil]);
        }
    }
    public function cetak_struk()
    {
        $jum = tb::count();

        if ($jum == 0) {
            $flight = new tb;



            $jumlah = tb::count() + 1;
            $randomNumber = rand(1, 1000);

            $flight->nomer = $jumlah;
            $flight->kode = $randomNumber;
            $flight->status = false;
            $flight->save();

            $jumm = tb::count();

        } else {

            $jumm = tb::count();
        }
        $hasil = DB::select('select * from antrian where nomer = ?', [$jumm]);
        // dd($hasil);




        $data = costum::find(1);
        // $hasil[0];
        $hari = date('l');
        $tanggal = date('Y-m-d');
        $jam = date('H:i:s');

        return view('struknya', ['nomer' => $hasil[0], 'tgl' => $tanggal, 'jam' => $jam, 'data' => $data]);

    }


    public function tambah_struk()
    {
        $flight = new tb;



        $jumlah = tb::count() + 1;
        $randomNumber = rand(1, 1000);

        $flight->nomer = $jumlah;
        $flight->kode = $randomNumber;
        $flight->status = false;
        $flight->save();

        $jumm = tb::count();
        $hasil = DB::select('select * from antrian where nomer = ?', [$jumm]);
        // dd($hasil);




        $data = costum::find(1);
        // $hasil[0];
        $hari = date('l');
        $tanggal = date('Y-m-d');
        $jam = date('H:i:s');

        return view('struknya', ['nomer' => $hasil[0], 'tgl' => $tanggal, 'jam' => $jam, 'data' => $data]);



    }


    public function tambah_antrian()
    {
        $flight = new tb;



        $jumlah = tb::count() + 1;
        $randomNumber = rand(1, 1000);

        $flight->nomer = $jumlah;
        $flight->kode = $randomNumber;
        $flight->status = false;
        $flight->save();

        return redirect('/admin/petugas');
    }

    public function logout()
    {
        auth::logout();
        return redirect('/');
    }
    public function logout_petugas()
    {
        auth::logout();
        return redirect('/');
    }

    public function scan($nomer, $kode)
    {

        if ($kode < 1) {
            $read = tb::count();
            $flight = new tb;
            $jumlah = tb::count() + 1;
            $randomNumber = rand(1, 1000);

            $flight->nomer = $jumlah;
            $flight->kode = $randomNumber;
            $flight->status = false;
            $flight->save();

            // $item = tb::where('nomer', $nomer)->where('kode', $read)->first();

            $read = tb::count();
            $izin = izin::find(1);
            $data = costum::find(1);
            $hasil = tb::where('nomer', $read)->first();

            $countTrueStatus = tb::where('status', true)->count();

            // dd($hasil);
            return view('scan_barcode', ['nomer' => $read, 'data' => $data, 'izin' => $izin, 'item' => $hasil, 'status' => $countTrueStatus]);

        } else {









            $read = tb::count();
            $izin = izin::find(1);
            $data = costum::find(1);
            $hasil = tb::where('nomer', $read)->first();


            // dd($read);
            return view('scan_barcode', ['nomer' => $read, 'data' => $data, 'izin' => $izin, 'item' => $hasil]);
        }

    }



    public function tambah_scan($nomer, $kode)
    {


        // if ($kode < 0) {



        $read = tb::count();
        $flight = new tb;
        $jumlah = tb::count() + 1;
        $randomNumber = rand(1, 1000);

        $flight->nomer = $jumlah;
        $flight->kode = $randomNumber;
        $flight->status = false;
        $flight->save();
        $read = tb::count();
        $izin = izin::find(1);
        $data = costum::find(1);
        $hasil = tb::where('nomer', $read)->first();

  $domainName = $_SERVER['SERVER_NAME'];

        // dd($read);
        return view('scan_barcode', ['nomer' => $read, 'data' => $data, 'izin' => $izin, 'item' => $hasil, 'server' => $domainName]);






    }


    public function scann($nomer, $kode)
    {
        $countTrueStatus = tb::where('status', true)->count();
        $item = tb::where('nomer', $nomer)->where('kode', $kode)->first();
        if ($item) {
            $countTrueStatus = tb::where('status', true)->count();





            // dd($countTrueStatus);

            $data = costum::find(1);
            return view('hasil_browser', ['data' => $data, 'nomer' => $nomer, 'kode' => $kode, 'antrian' => $countTrueStatus]);
        } else {
            echo "kode dan nomer anda salah";
        }


    }



    ////admin
    public function admin_antrian()
    {
        $semua = tb::all();
        $izin = izin::find(1);
        $data = costum::find(1);
        // dd($izin);
        return view('adantrian', ['users' => $semua, 'izin' => $izin, 'data' => $data]);
    }

    public function status_hijau($data)
    {
        tb::where('nomer', $data)
            ->update(['status' => false]);


        $jumlah = tb::where('status', true)->count();
        // dd($jumlah);
        event(new message($jumlah));
        return redirect('/admin/antrian');
    }
    public function status_kuning($data)
    {
        tb::where('nomer', $data)
            ->update(['status' => true]);


        $jumlah = tb::where('status', true)->count();
        // dd($jumlah);
        event(new message($jumlah));



        return redirect('/admin/antrian');


    }

    public function buka_antrian()
    {
        izin::where('id', 1)
            ->update(['izin' => true]);



        event(new stop(1));
        return redirect('/admin/antrian');



    }
    public function stop_antrian()
    {
        izin::where('id', 1)
            ->update(['izin' => false]);

        event(new stop(1));
        return redirect('/admin/antrian');



    }

    public function ubah()
    {
        $data = costum::find(1);

        $izin = User::find(2);
        return view('ubah_user', ['nama' => $izin, 'data' => $data]);
    }
    public function ubah_admin(request $request)
    {




        $messages = [
            'required' => 'Kolom :attribute harus diisi.',
            'string' => 'Kolom :attribute harus berupa teks.',
            'max' => 'Kolom :attribute tidak boleh lebih dari :max karakter.',
            'email' => 'Format :attribute tidak valid.',
            'unique' => ':attribute sudah digunakan.',
            'min' => 'Kolom :attribute harus memiliki minimal :min karakter.',
            'confirmed' => 'Konfirmasi :attribute tidak cocok.'
        ];
        $validator = Validator::make($request->all(), [

            'pasword' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return redirect('/ubah/user/admin')
                ->withErrors($validator)
                ->withInput();
        }







        // dd($request['pasword']);
        User::where('id', 2)
            ->update(['password' => Hash::make($request['pasword'])]);
        return redirect('/admin/antrian');

    }

    public function ubah_petugas()
    {







        $data = costum::find(1);
        $izin = User::find(1);
        return view('ubah_petugas', ['nama' => $izin, 'data' => $data]);
    }

    public function ubah_pt(request $request)
    {



        $messages = [
            'required' => 'Kolom :attribute harus diisi.',
            'string' => 'Kolom :attribute harus berupa teks.',
            'max' => 'Kolom :attribute tidak boleh lebih dari :max karakter.',
            'email' => 'Format :attribute tidak valid.',
            'unique' => ':attribute sudah digunakan.',
            'min' => 'Kolom :attribute harus memiliki minimal :min karakter.',
            'confirmed' => 'Konfirmasi :attribute tidak cocok.'
        ];
        $validator = Validator::make($request->all(), [

            'pasword' => 'required',
        ], $messages);
        if ($validator->fails()) {
            return redirect('/ubah/user/petugas')
                ->withErrors($validator)
                ->withInput();
        }





        // dd($request['pasword']);
        User::where('id', 1)
            ->update(['password' => Hash::make($request['pasword'])]);
        return redirect('/admin/antrian');

    }

    public function costum()
    {
        $data = costum::find(1);
        return view('costum', ['data' => $data]);
    }

    public function costum_upload(request $request)
    {

        $validator = Validator::make($request->all(), [
            'nama' => 'required|string|max:255',
            'kalimat' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif', // Validasi untuk file gambar
        ], [
            'required' => 'Kolom :attribute harus diisi.',
            'string' => 'Kolom :attribute harus berupa teks.',
            'max' => 'Kolom :attribute tidak boleh lebih dari :max karakter.',
            'image' => 'Kolom :attribute harus berupa file gambar dengan format jpeg, png, jpg, atau gif.',
            'mimes' => 'Kolom :attribute harus berupa file dengan format :values.',
            'max' => 'Kolom :attribute tidak boleh lebih dari :max kilobyte.',
        ]);

        if ($validator->fails()) {
            return redirect('/costum')
                ->withErrors($validator)
                ->withInput();
        }

        $imageName = time() . '.' . $request->image->extension();
        // dd($request['nama']);
        // $request->image->move(public_path('images'), $imageName);
        Storage::disk('public')->put('images/' . $imageName, file_get_contents($request->image));


        $costum = costum::find(1);


        $costum->nama_puskesmas = $request['nama'];
        $costum->pembuka = $request['kalimat'];
        $costum->foto = $imageName;

        $costum->save();
        return redirect('/admin/antrian');
    }

    public function reset()
    {
        $deleted = DB::table('antrian')->delete();



        event(new stop(1));
        return redirect('/admin/antrian');
    }


    public function eror()
    {
        $read = tb::count();
        $izin = izin::find(1);
        $data = costum::find(1);
        return view('eror', ['nomer' => $read, 'data' => $data, 'izin' => $izin]);
    }

}
