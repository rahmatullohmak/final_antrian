<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\antrian;
use App\http\Middleware\tingkat;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::middleware(['guest'])->group(function () {

    Route::get('/', [antrian::class, 'awal']);


    Route::get('/login', [antrian::class, 'login'])->name('login');


    Route::post('/login/kirim', [antrian::class, 'aut']);

});

route::get('/eror', [antrian::class, 'eror']);

Route::get('/barcode_nomer/{nomer}/{kode}', [antrian::class, 'scann']);


Route::middleware(['auth'])->group(function () {
    // ...



    //petugas



    Route::middleware(['tingkat:petugas'])->group(function () {

        Route::get('/admin/petugas', [antrian::class, 'petugas_antrian']);


        Route::get('/struk', [antrian::class, 'tambah_struk']);

        Route::get('/tambah', [antrian::class, 'tambah_antrian']);

        Route::get('/logout_petugas', [antrian::class, 'logout_petugas']);
        Route::get('/barcode/{nomer}/{kode}', [antrian::class, 'tambah_scan']);
    });



    //admin antrian


    Route::middleware(['tingkat:admin'])->group(function () {
        Route::get('/admin/antrian', [antrian::class, 'admin_antrian']);
        Route::put('/status/hijau/{data}', [antrian::class, 'status_hijau']);
        Route::put('/status/kuning/{data}', [antrian::class, 'status_kuning']);
        Route::get('/buka', [antrian::class, 'buka_antrian']);
        Route::get('/stop', [antrian::class, 'stop_antrian']);
        Route::get('ubah/user/admin', [antrian::class, 'ubah']);
        Route::get('ubah/user/petugas', [antrian::class, 'ubah_petugas']);
        Route::put('/ubah/admin', [antrian::class, 'ubah_admin']);
        Route::put('/ubah/petugas', [antrian::class, 'ubah_pt']);
        Route::get('/costum', [antrian::class, 'costum']);
        Route::put('/costum', [antrian::class, 'costum_upload']);

        Route::get('/logout', [antrian::class, 'logout']);
        Route::get('/reset', [antrian::class, 'reset']);





    });

});

